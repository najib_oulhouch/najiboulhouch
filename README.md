### Hello everybody, welcome to my GitHub Profil 👋

Hello, I'm **NAJIB OULHOUCH** from Morocco. I'm **Performance Test Engineer** and **FullStack Developer(Spring boot, Symfony)**.

I started my career as **Symfony developer** in 2013, as intern in a company :office: I developed a monolith web applications using Framework Symfony2. <br> </br>
Below a application's repositories : </br>
https://github.com/najiboulhouch/moviesManagement. </br>
https://github.com/najiboulhouch/publication. </br>

In 2014, i found out a beautiful Framework that changed my programmig mindset  :heart: **Spring framework** :heart: ,hence i learned it quickly by practice and development of a robust web applications with Spring. </br>

I shared with you my profils for more informations :wink: : 

<p>
  <a href="https://www.linkedin.com/in/najib-oulhouch/" target="_blank"><img alt="My LinkdeIN" src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" /></a>
      <a href="https://www.youtube.com/channel/UCOc8j3MEIG2jhWl5WqoI4RQ" target="_blank"><img alt="My WebSite" src="https://img.shields.io/badge/YouTube-FF0000?style=for-the-badge&logo=youtube&logoColor=white" /></a>
      <a href="http://najiboulhouch.net/" target="_blank"><img alt="My WebSite" src="https://img.shields.io/website-up-down-green-red/http/monip.org.svg" /></a>

</p>




  ## Languages and Tools 
![Java](	https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white)
![PHP](	https://img.shields.io/badge/PHP-777BB4?style=for-the-badge&logo=php&logoColor=white)
![JavaScript](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)
![HTML](https://img.shields.io/badge/HTML-239120?style=for-the-badge&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)
![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
![Angular](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white)
![Bootstrap](https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white)
![Heroku](https://img.shields.io/badge/Heroku-430098?style=for-the-badge&logo=heroku&logoColor=white)



## My Git References 💻

![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=najiboulhouch&exclude_repo=github-readme-statt&layout=compac)

![Anurag's github stats](https://github-readme-stats.vercel.app/api?username=najiboulhouch&show_icons=true&theme=dracula&repo=github-readme-stats)
